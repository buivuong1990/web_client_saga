import React from 'react';

import {ContainerStyle} from './container.styles';

export class Container extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            
        }
    }
    render(){
        const {children} = this.props;
        return (
            <ContainerStyle>
                {children}
            </ContainerStyle>
        )
    }
}