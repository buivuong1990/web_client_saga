import styled from 'styled-components';

export const ContainerStyle = styled.div`
    padding-left: 40px;
    padding-right: 40px;
`;