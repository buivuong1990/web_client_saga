import styled from 'styled-components';

export const WrapperStyle = styled.div`
    margin-bottom: 0;
    color: #ffffff;
    justify-content: space-between;
    display: flex;
`;