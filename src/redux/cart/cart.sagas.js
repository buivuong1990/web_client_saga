import { takeEvery } from 'redux-saga/effects';

import CartActionTypes from './cart.types';

export function* fetchOrderItemsAsync(){
    yield console.log('I am fired');
}

export function* orderCreated(){
    yield takeEvery(
        CartActionTypes.ADD_ITEM, 
        fetchOrderItemsAsync
    );
}