import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyDxQhEONfHT1_RqpLS8OuBpcu-TUqdQyis",
    authDomain: "test-db-281ef.firebaseapp.com",
    databaseURL: "https://test-db-281ef.firebaseio.com",
    projectId: "test-db-281ef",
    storageBucket: "",
    messagingSenderId: "409730194151",
    appId: "1:409730194151:web:bdcc71035a38188d6a6042",
    measurementId: "G-6KRERWBBRZ"
}

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapShot = await userRef.get();

    if(!snapShot.exists){
        const { displayName, email } = userAuth;
        const createdAt = new Date();
        try{
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            });
        }catch(error){
            console.log('error creating user', error.message);
        }
    }

    return userRef;
}

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;