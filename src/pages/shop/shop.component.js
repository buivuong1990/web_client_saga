import React from 'react';

import {Route} from 'react-router-dom';

import CollectionOverviews from  '../../components/collections-overview/collections-overview.component';
import Category from '../category/category.component';

import WithSpinner from '../../components/with-spinner/with-spinner.component';

const CollectionOverviewsWithSpinner = WithSpinner(CollectionOverviews);

class ShopPage extends React.Component{
    constructor(){
        super();
        this.state = {
            isLoading: false
        }
    }
    render(){
        const {match} = this.props
        return (
            <div className="shop-page">
                <Route exact path={`${match.path}`} render={(props) => <CollectionOverviewsWithSpinner isLoading={this.state.isLoading} {...props}/>}/>
                <Route path={`${match.path}/:categoryId`} component={Category}/>
            </div>
        )
    }
}

export default ShopPage;