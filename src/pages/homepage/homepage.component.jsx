import React from 'react';

//import {ThemeProvider} from 'styled-components';

/*const theme = {
    main: 'mediumseagreen'
}*/

import {Container} from '../../theme/dingtoi/container/container.component';
import {Navbar} from '../../theme/dingtoi/navbar/navbar.component';

const Homepage = () => (
    <Container>
        <Navbar/>
    </Container>
)

export default Homepage;